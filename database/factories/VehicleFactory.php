<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vehicle;
use Faker\Generator as Faker;

$factory->define(Vehicle::class, function (Faker $faker) {
    return [
        'sector_id' => function () {
            return factory(App\Models\Sector::class)->create()->id;
        },
        'vehicle_type_id' => function () {
            return factory(App\Models\VehicleType::class)->create()->id;
        },
        'user_id' => function () {
            $u = factory(App\Models\User::class)->create()->assignRole('usuario');
            return $u->id;
        },
        'model_id' => function () {
            return factory(App\Models\Model::class)->create()->id;
        },
        
        'color_id' => function () {
            return factory(App\Models\Color::class)->create()->id;
        },
        'displacement_id' => function () {
            return factory(App\Models\Displacement::class)->create()->id;
        },
        
        'fuel' => $faker->randomElement($array = array ('Diésel', 'Gasolina', 'Gas')),
        
        'description' => $faker->paragraph,
        'price' => $faker->randomNumber(),
        'doors' => $faker->randomElement($array = array ('2', '3', '4')),
        'year' => $faker->randomElement($array = array ('2000', '2001', '2003', '2005')),
        'mileage' => $faker->randomNumber(),
        'transmission' => $faker->randomElement($array = array ('Manual', 'Automática')),
        'is_active' =>  $faker->randomElement($array = array (true, false, true, true)),
        'status' => 'Publicado',
    ];
});
