<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Http\Resources\VehicleCollection;
use App\Http\Resources\Vehicle as VehicleResource;

class VehiclesController extends Controller
{
    public function index()
    {
       $vehicles = Vehicle::with('sector.state', 'model.brand', 'media')
                ->where('status', 'Publicado')
                ->model(request()->q)
                ->type(request()->vehicle_type_id)
                ->brand(request()->brand_id)
                ->modelId(request()->model_id)
                ->color(request()->color_id)
                ->fuel(request()->fuel)
                ->yearFrom(request()->year_from)
                ->yearTo(request()->year_to)
                ->priceFrom(request()->price_from)
                ->state(request()->state_id)
                ->sector(request()->sector_id)
                ->priceTo(request()->price_to)
                ->transmission(request()->transmission)
                ->displacement(request()->displacement_id)
                ->order(request()->order)
                ->paginate(20);

       $vehicles = new VehicleCollection($vehicles);
        
       return view('vehicles.index', compact('vehicles'));
    }

    public function show($vehicle)
    {
        $vehicle = Vehicle::with('sector.state', 'model.brand', 'user', 'color', 'displacement', 'vehicleType', 'messages')
                ->find($vehicle);

        return view('vehicles.show', compact('vehicle'));
    }
}
