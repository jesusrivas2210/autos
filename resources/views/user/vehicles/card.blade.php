
<div class="mb-2 flex bg-white rounded-lg  overflow-hidden border border-gray-500 block text-gray-800">
    <img class="p-1 w-24 h-24 sm:h-16 sm:w-16 object-cover object-center" src="{{ $vehicle->firstMediaUrl() }}" >
    <div class="p-1 sm:flex items-center w-full">
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6">
        <a class="underline" href="{{ route('user.vehicles.edit', ['vehicle' => $vehicle->id]) }}">
                {{ $vehicle->model->brand->name }} {{ $vehicle->model->name }}
            </a>
            
        </div>
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6">{{ $vehicle->year }}</div>
        <div class="hidden md:block md:w-1/5 lg:w-1/6">{{ $vehicle->sector->name }}, {{ $vehicle->sector->state->name }}</div>
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6" >Publicado {{ $vehicle->created_at->diffForHumans() }}</div>
        <div class="  hidden lg:block lg:w-1/6"> <span class="">{{ setting('site.currency') }}</span> {{number_format($vehicle->price/100, 2, 

            setting('site.decimal_point'), setting('site.thousands_separator'))}}
        </div>
    <div  class="sm:w-1/4 md:w-1/5 lg:w-1/6">{{ $vehicle->status }}</div>
    </div>
</div> 

