<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Http\Requests\StoreProfile;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(StoreProfile $request)
    {
        $user = request()->user();

        $profile = new Profile;
        $profile->fill($request->all());
        $user->profile()->save($profile);

        return redirect()
                    ->route('user.profile')
                    ->with('success', 'Información guardada con éxito.');
    }

    public function show()
    {
        $user = request()->user();
        $profile = $user->profile;

        return view('user.profile.show', compact('user', 'profile'));
    }

    public function create()
    {
        $user = request()->user();
        $profile = $user->profile;
        if ($profile) {
            return redirect()->route('user.profile.edit');
        }

        return view('user.profile.create', compact('user', 'profile'));
    }

    public function edit()
    {
        $user = request()->user();
        $profile = $user->profile;
        
        return view('user.profile.edit', compact('user', 'profile'));
    }

    public function update(StoreProfile $request)
    {
        $user = request()->user();
        $user->profile->fill($request->all());
        $user->profile->save();

        return redirect()
                ->route('user.profile')
                ->with('success', 'Información guardada con éxito.');
    }
}
