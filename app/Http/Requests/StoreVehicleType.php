<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreVehicleType extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'types.store') {
            return [
                'name' => 'required|unique:vehicle_types|max:64',
            ];
        }

        return [

            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('vehicle_types')->ignore($this->route('type'))]
        ];
    }
}
