<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreState extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'states.store') {
            return [
                'name' => 'required|unique:states|max:64',
                'country_id' => 'required|integer',
            ];
        }

        return [
            'country_id' => 'required|integer',
            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('states')->ignore($this->route('state'))]
        ];
    }
}
