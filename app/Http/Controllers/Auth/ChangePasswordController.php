<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\Rules\SamePassword;
use App\Rules\PasswordMatches;
use App\User;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showChangePasswordForm()
    {
        return view('auth.changepassword');
    }
    
    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new PasswordMatches],
            'password' => ['required', 'string', 'min:8', 'confirmed', 
                new SamePassword($request->current_password)],
        ]);
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('user.profile')->with('success', 'Contraseña cambiada con éxito.');
    }
}
