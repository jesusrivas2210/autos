<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->comment('usuario propietario del concesionario');
            $table->bigInteger('sector_id')->unsigned();
            $table->string('name')->commet('nombre o razon social del concesionario');
            $table->string('dni')->commet('cedula o rif del concesionario');
            $table->text('fiscal_address')->comment('direccion fiscal del concesionario');
            $table->text('address')->comment('direccion alternativa del concesionario');
            $table->string('postal_zone')->comment('zona postal del concesionario');
            $table->string('phone')->comment('telefono del concesionario');
            $table->string('celphone')->comment('celular del concesionario');
            $table->string('alt_phone')->nullable()->comment('telefono alternativo del concesionario');
            $table->string('email')->comment('correo del concesionario');
            $table->string('alt_email')->nullable()->comment('correo alternativo del concesionario');          
            $table->string('street')->comment('calle del concesionario');
            $table->enum('building', ['Edificio', 'Local', 'Casa'])->comment('edificio del concesionario');
            $table->string('building_number')->comment('numero del edificio del concesionario'); 
            $table->boolean('is_active')->default(false)->comment('si esta activo se puede seleccionar'); 
            $table->timestamps();
            $table->string('status');
            $table->foreign('sector_id')->references('id')->on('sectors');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garages');
    }
}
