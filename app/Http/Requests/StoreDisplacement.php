<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreDisplacement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'displacements.store') {
            return [
                'name' => 'required|unique:displacements|max:64',
            ];
        }

        return [

            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('displacements')->ignore($this->route('displacement'))]
        ];
    }
}
