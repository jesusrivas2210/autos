<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Vehicle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'fuel' => $this->fuel,
            'description' => $this->description,
            'price' => $this->price,
            'doors' => $this->doors,
            'year' => $this->year,
            'mileage' => $this->mileage,
            'transmission' => $this->transmission,
            'is_active' => $this->is_active,
            'model' => new Model($this->whenLoaded('model')),
            'type' => new VehicleType($this->whenLoaded('vehicleType')),
            'color' => new Color($this->whenLoaded('color')),
            'status' => $this->status,
            'sector' => new Sector($this->whenLoaded('sector')),
            'displacement'  => new Displacement($this->whenLoaded('displacement')),   
            'photosUrls' => $this->getMediaUrls('photos'),    
        ];
    }
}
