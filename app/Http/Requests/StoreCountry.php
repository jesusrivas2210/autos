<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreCountry extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'countries.store') {
            return [
                'name' => 'required|unique:countries|max:32',
                'decimal_point' => [ 'required', Rule::in([',', '.']) ],
                'thousands_separator' => [ 'required', Rule::in([',', '.']) ],
                'currency_code' => 'required|alpha|max:3',
                'locale' => 'required|alpha|max:3',
                'distance_symbol' => 'required|alpha|max:3',
                'state_name' => 'required|string|max:100',
                'sector_name' => 'required|string|max:100',
            ];
        }

        return [
            'decimal_point' => [ 'required', Rule::in([',', '.']) ],
            'thousands_separator' => [ 'required', Rule::in([',', '.']) ],
            'currency_code' => 'required|alpha|max:3',
            'locale' => 'required|alpha|max:3',
            'distance_symbol' => 'required|alpha|max:3',
            'state_name' => 'required|string|max:100',
            'sector_name' => 'required|string|max:100',
            'name' => [ 'required', 
                         'max:32',
                         Rule::unique('countries')->ignore($this->route('country'))]
        ];
    }
}
