<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreVehicle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_type_id' => 'required|integer',
            'model_id' => 'required|integer',            
            'color_id' => 'required|integer',
            'status' => 'required|string',
            'sector_id' => 'required|integer',
            'displacement_id' => 'nullable|integer',
            'fuel' => ['nullable', Rule::in(['Diésel', 'Gasolina', 'Gas', 'Eléctrico']) ],
            
            'description' => 'required|string|max:300',
            'price' => 'required|integer',
            'doors' => 'nullable|integer',
            'year' => 'required|integer|min:1950|max:2100',
            'mileage' => 'required|integer',
            'transmission' => ['nullable', Rule::in(['Manual', 'Automática'])],
        ];
    }
}
