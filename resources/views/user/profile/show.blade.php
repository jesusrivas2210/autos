@extends('layouts.app')

@section('title')Mi Cuenta | @endsection 

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap justify-center">
        <div class=" w-full sm:w-3/4  lg:w-1/2 xl:w-2/5 border border-gray-500  rounded overflow-hidden shadow-md m-4">
            <div class=" flex items-center px-4 justify-between border-b border-gray-500">
                <div class="flex items-center ">
                    <svg class="fill-current h-4 w-4"  viewBox="0 0 20 20"><path d="M5 5a5 5 0 0 1 10 0v2A5 5 0 0 1 5 7V5zM0 16.68A19.9 19.9 0 0 1 10 14c3.64 0 7.06.97 10 2.68V20H0v-3.32z"/></svg>
                    <h1 class="ml-3">Mi Cuenta</h1>
                </div>
                <div>
                @if($profile != null)
                    <div>
                        <a class="link" href="{{ route('user.profile.edit') }}">Editar</a>
                    </div>
                @endif   
                </div>        
            </div>
            <div class="sm:flex  p-4">
                <div class="mb-4 sm:mb-0">
                    <profile-photo @avatarchanged="avatar()" :avatar_url="image_url"></profile-photo>

                </div>
                <div class="sm:ml-4">
                    
                    <h2>Datos Personales</h2>
                    <div class="text-sm">
                        <div>
                            <label class="font-semibold">Nombres y Apellidos:</label> 
                            {{ $user->name }} {{ $user->lastname }}
                        </div>
                        
                        <div class="mt-2">
                            <label class="font-semibold">Correo Electrónico:</label> 
                            {{ $user->email }}
                        </div>

                        @if($profile)
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.dni_type') }}:</label>
                            {{ $profile->dni }}
                        </div>

                        <div class="mt-2">
                            <label class="font-semibold">Teléfono:</label>
                            {{ $profile->phone }}
                        </div>
                    </div>
                    <h2 class="mt-4">Datos de Ubicación</h2>
                    <div class="text-sm">
                        <div>
                            <label class="font-semibold">Dirección:</label>
                            {{ $profile->address }}
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">Ciudad:</label>
                            {{ $profile->city }}
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.state_name') }}:</label>
                            {{ $profile->sector->state->name }}
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.sector_name') }}:</label>
                            {{ $profile->sector->name }}
                        </div>
                        <h2 class="mt-4">Métodos de Pago</h2>
                        <div>

                        </div> 
                        @else 
                        <div class="mt-4">
                            <p class="p-2 bg-green-200 border-l-4 border-green-700">Completa los datos de tu cuenta de usuario  
                                <a class="link" href="{{ route('user.profile.create') }}">aquí</a>
                            </p>
                        </div> 
                        @endif  
                    </div>
                              
                </div>
            </div>
        </div>
    </div>
</div>
@endsection