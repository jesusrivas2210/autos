<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreGarage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'dni' => 'required|integer',
            'fiscal_address' => 'required|string|max:200',
            'address' => 'required|string|max:200',
            'phone' => 'required|integer',
            'postal_zone' => 'required|integer',
            'celphone' => 'required|integer',
            'alt_phone' => 'required|integer',
            'alt_email' => 'required|email|max:100',
            'email' => 'required|email|max:100',
            'sector_id' => 'required|integer',
            'street' => 'required|string|max:100',
            'building' => [
                    'required',
                    Rule::in(['Edificio', 'Local', 'Casa']),
                    ],
            'building_number' => 'required|string|max:100',
        ];
    }
}
