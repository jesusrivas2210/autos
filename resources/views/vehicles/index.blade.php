@extends('layouts.app')

@section('title')Buscar vehículo | @endsection 

@section('content')
<div class="">
    
    
    <div class="sm:flex sm:flex-wrap">
        
        <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 bg-gray-300 sm:border-r sm:border-b sm:border-gray-500">
            <form class="" action="/vehicles" id="filter-form">
                
                <div class="border-b border-gray-500 p-4 flex items-center">                    
                    <input class="mr-0 px-1  rounded-l-lg border border-gray-500" name="q" value="{{ request()->q }}" placeholder="Buscar vehículo...">
                    <button class="ml-0 mr-3 py-1 px-1 border border-l-0 border-gray-500 bg-blue-200 rounded-r-lg">
                        <svg class="fill-current w-4 h-4 " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/></svg>
                    </button>     
                                   
                </div> 
                
                <div class="w-full p-4"  >
                    <h3 class="">Ordenar resultados por:</h3>
                    <order-filter :old="'{{ request()->order }}'" @order-changed="submitFilter()"></order-filter>                    
                
                    <h3 class="mt-4">Filtrar resultados por:</h3>
                    <label class="mt-1  block text-sm">Tipo de vehículo</label>
                    <select-vehicle-type @type-selected="setTypeId($event)" :old="'{{ request()->vehicle_type_id }}'" ></select-vehicle-type>
                    
                    <label class="mt-2  block text-sm">Marca</label>
                    <select-brand @brand-selected="setBrandId($event)" :old="'{{ request()->brand_id }}'"></select-brand>

                    <label v-if="brand_id != ''" class="mt-2  block text-sm">Modelo</label>
                    <select-model v-if="brand_id != ''" :brand_id="brand_id" :old="'{{ request()->model_id }}'"></select-model>

                    <label class="mt-2  block text-sm">Color</label>
                    <select-color :old="'{{ request()->color_id }}'"></select-color>

                    <label v-if="type_id==3" class="mt-2  block text-sm">Cilindrada</label>
                    <select-displacement v-if="type_id==3" :old="'{{ request()->displacement_id }}'"></select-displacement>
                    
                    <label class="mt-2  block text-sm">Combustible</label>
                    <select-fuel :old="'{{ request()->fuel }}'"></select-fuel>

                    <label class="mt-2  block text-sm">Transmisión</label>
                    <select-transmission :old="'{{ request()->transmission }}'"></select-transmission>

                    <label class="mt-2  block text-sm">Año</label>
                    <select-year :old_year_from="'{{ request()->year_from }}'" :old_year_to="'{{ request()->year_to }}'"></select-year>

                    <label class="mt-2  block text-sm">Precio</label>
                    <select-price :old_price_from="'{{ request()->price_from }}'" :old_price_to="'{{ request()->price_to }}'"></select-price>

                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.state_name') }})</label>
                    <select-state @state-selected="setStateId($event)" :old="'{{ request()->state_id }}'" 
                        class="g7_select"></select-state>
                        
                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.sector_name') }})</label>
                    <select-sector :state_id="state_id" 
                                :old="'{{ request()->sector_id }}'"
                                class="g7_select"></select-sector>


                    <button class="mt-6 btn w-full ">Aplicar filtros</button>

                    <a href="/vehicles" class="link">Quitar filtros</a>
                </div>
                
            </form>
            
        </div>
        <div class="w-full sm:w-1/2 md:w-2/3 lg:w-3/4 xl:w-4/5">
            
            @if(count($vehicles) > 0)
            <div class="text-gray-700 text-sm p-2">
                {{ 1 + (($vehicles->currentPage()-1) * 20) }} al {{ $vehicles->total() >= 20 ? ((($vehicles->currentPage()-1) * 20) + 20) : $vehicles->total() }} de {{ $vehicles->total() }} resultados
            </div>
            <div class="flex flex-wrap -mb-4 ">
                @foreach($vehicles as $vehicle)
                <div class="p-1 sm:w-full md:w-1/2 lg:w-1/3 xl:w-1/3">
                    @include('vehicles.card',['vehicle' => $vehicle])
                </div>
                @endforeach
            </div>
            @else 
            <div class="text-red-700 p-4">Sin resultados</div>
            @endif
            
            
            <div class="my-4 mx-1 ">
                {{ $vehicles->appends(request()->query())->links('vendor.pagination.tailwind') }}
            </div>
        </div>
        
    </div>
</div>
@endsection