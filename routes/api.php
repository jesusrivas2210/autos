<?php

Route::get('/states', 'MasterDataController@states');
Route::get('/states/{state}/sectors', 'MasterDataController@sectors');
Route::get('/vehicle-types', 'MasterDataController@vehicleTypes');
Route::get('/colors', 'MasterDataController@colors');
Route::get('/displacements', 'MasterDataController@displacements');
Route::get('/brands', 'MasterDataController@brands');
Route::get('/brands/{brand}/models', 'MasterDataController@models');
Route::get('/user/profile/avatar-url', 'ProfileImageController@avatarUrl');
Route::post('/user/profile/avatar', 'ProfileImageController@avatar');
Route::post('/vehicles/{vehicle}/photos', 'VehicleImagesController@store');
Route::get('/vehicles/{vehicle}/photos', 'VehicleImagesController@index');
Route::delete('/vehicles/{vehicle}/photos/{photo}', 'VehicleImagesController@delete');