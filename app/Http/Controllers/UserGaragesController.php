<?php

namespace App\Http\Controllers;

use App\Models\Garage;
use App\Http\Requests\StoreGarage;

class UserGaragesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $user = request()->user();
        $garages = Garage::where('user_id', $user->id)->paginate();

        return view('user.garages.index', compact('garages'));
    }

    public function create()
    {
        return view('user.garages.create');
    }

    public function store(StoreGarage $request)
    {
        $user = request()->user();

        $garage = new Garage;
        $garage->fill($request->all());
        $user->garages()->save($garage);

        return redirect()
                ->route('user.garages.edit', ['garage' => $garage->id])
                ->with('success', 'Concesionario registrado con éxito.');
    }

    public function edit($garage)
    {
        $user = request()->user();
        $garage = Garage::where('user_id', $user->id)
                    ->where('id', $garage)
                    ->first();

        return view('user.garages.edit', compact('garage'));
    }
    
    public function update(StoreGarage $request, $garage)
    {
        $user = request()->user();
        $garage = Garage::where('user_id', $user->id)
                    ->where('id', $garage)
                    ->first();
        $garage->update($request->all());

        return redirect()
                ->route('user.garages.edit', ['garage' => $garage->id])
                ->with('success', 'Concesionario actualizado con éxito.');
    }
   
}