<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Brand extends Eloquent
{
    protected $guarded = [];

    public function models()
    {
        return $this->hasMany(Model::class);
    }
}
