<div class=" w-full bg-white rounded-lg shadow-lg overflow-hidden border border-gray-500 inline-block text-gray-800">
    <div class=" w-full">
      <img class="block object-cover object-center w-auto h-auto sm:h-48 sm:w-full" src="{{ $vehicle->firstMediaUrl() }}" >
      
    </div>
    <div class="p-2 w-full" >
        <div class="font-semibold text-lg">
            <a href="{{ route('vehicles.show', ['vehicle' => $vehicle->id]) }}" class="hover:underline">{{ $vehicle->model->brand->name }} {{ $vehicle->model->name }}</a>
        </div>
        <span class="text-xs" >Publicado {{ $vehicle->created_at->diffForHumans() }}</span>
        <div class="mt-2">{{ $vehicle->year }}</div>
        <div class="text-sm">{{ $vehicle->sector->name }}, {{ $vehicle->sector->state->name }}</div>
        <div class="text-xl mt-2"><span class="text-base">{{ setting('site.currency') }}</span> {{number_format($vehicle->price/100, 2, 
            setting('site.decimal_point'), setting('site.thousands_separator'))}}</div>
    </div>
</div> 

