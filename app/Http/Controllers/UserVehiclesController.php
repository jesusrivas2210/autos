<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\User;
use App\Http\Resources\VehicleCollection;
use App\Http\Resources\Vehicle as VehicleResource;
use App\Http\Requests\StoreVehicle;

class UserVehiclesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = request()->user();
        $vehicles = Vehicle::where('user_id', $user->id)->paginate(20);
        $vehicles = new VehicleCollection($vehicles);
        
        return view('user.vehicles.index', compact('vehicles'));
    }

    public function create()
    {
        return view('user.vehicles.create');
    }

    public function edit($vehicle)
    {
        $user = request()->user();
        $vehicle = Vehicle::with(['model', 'sector'])
                    ->where('user_id', $user->id)
                    ->where('id', $vehicle)
                    ->first();

        return view('user.vehicles.edit', compact('vehicle'));
    }

    public function store(StoreVehicle $request)
    {
        $user = request()->user();
        $vehicle = new Vehicle;
        $vehicle->fill($request->all());
        $user->vehicles()->save($vehicle);

        $vehicle = new VehicleResource($vehicle);

        return redirect()
                ->route('user.vehicles.edit', ['vehicle' => $vehicle->id])
                ->with('success', 'Vehículo publicado con éxito.');
    }

    public function update(StoreVehicle $request, $vehicle)
    {
        $user = request()->user();
        $vehicle = Vehicle::where('user_id', $user->id)
                    ->where('id', $vehicle)
                    ->first();
        $vehicle->update($request->all());

        $vehicle = new VehicleResource($vehicle);

        return redirect()
                ->route('user.vehicles.edit', ['vehicle' => $vehicle->id])
                ->with('success', 'Vehículo actualizado con éxito.');
    }

}
