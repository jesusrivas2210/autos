<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SamePassword implements Rule
{
    private $currentPassword;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strcmp($this->currentPassword, $value) != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.not_same_password');
    }
}
