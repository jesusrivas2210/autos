<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StorePaymentMethod extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'payment_methods.store') {
            return [
                'name' => 'required|unique:payment_methods|max:64',
            ];
        }

        return [

            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('payment_methods')->ignore($this->route('payment_method'))]
        ];
    }
}
