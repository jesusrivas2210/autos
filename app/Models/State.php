<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = [];

    public function sectors()
    {
        return $this->hasMany(Sector::class);
    }
}
