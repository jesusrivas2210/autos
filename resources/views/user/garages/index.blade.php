@extends('layouts.app')

@section('title')Mis concesionarios | @endsection 

@section('content')
<div class="container mx-auto">
    <div class="mx-1 sm:flex flex-wrap items-center justify-between mb-3">
        <h1 class="my-4 ">Mis concesionarios</h1>
        <a class="py-1 px-3 hover:bg-green-600 text-sm rounded-lg bg-green-400 border border-green-600 text-white" href="{{ route('user.garages.create') }}">Registrar concesionario</a>    
    </div>
    <div class="mx-auto flex flex-wrap justify-center">
            
        <div class="w-full">            
            @foreach($garages as $garage)
            <div class="mx-1">
                @include('user.garages.card',['garage' => $garage])</div>            
            @endforeach            
        </div>         

        <div class="my-4 mx-1 ">
            {{ $garages->appends(request()->query())->links('vendor.pagination.tailwind') }}
        </div>
    </div>    
</div>
@endsection