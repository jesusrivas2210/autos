<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMessage;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(StoreMessage $request, $vehicle)
    {
        $user = $request->user();
        $vehicle = Vehicle::find($vehicle);
        $message = new Message;
        $message->question = $request->question;
        $message->user_id = $user->id;
        $message->is_active = true;
        $vehicle->messages()->save($message);

        return redirect()
            ->route('vehicles.show', ['vehicle' => $vehicle->id])
            ->with('success', 'Pregunta enviada al vendedor.');
    }

    public function update(StoreMessage $request, $message)
    {
        $message = Message::find($message);
        $message->answer = $request->question;
        $message->answered_at = now();
        $message->save();

        return redirect()
            ->route('vehicles.show', ['vehicle' => $message->vehicle_id])
            ->with('success', 'Respuesta enviada al comprador.');
    }
}
