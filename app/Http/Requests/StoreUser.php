<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'users.store') {
            return [
                'name' => 'required|max:64',
                'lastname' => 'required|max:64',
                'email' => 'required|email|unique:users|max:100',
            ];
        }

        return [
            'name' => 'required|max:64',
            'lastname' => 'required|max:64',
            'email' => [ 'required', 
                         'email',
                         'max:100',
                         Rule::unique('users')->ignore($this->route('user'))]
        ];
    }
}
