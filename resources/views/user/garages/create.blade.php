@extends('layouts.app')

@section('title')Registrar concesionario | @endsection 

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap justify-center">
        <div class=" w-full sm:w-3/4  lg:w-1/2 xl:w-2/5 border border-gray-500  rounded overflow-hidden shadow-md m-4">
            <div class=" flex items-center px-4 justify-between border-b border-gray-500">
                <div class="flex items-center ">
                    <svg class="fill-current h-4 w-4"  viewBox="0 0 20 20"><path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/></svg>
                    <h1 class="ml-3">Registrar concesionario</h1>
                </div>                    
                <div>
                    <a class="link" href="/user/garages">Cancelar</a>                          
                </div>        
            </div>
            <div class="  p-4">     
                
                <div class="text-red-700">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                <form method="POST" action="{{ route('user.garages.store') }}" class="w-full">
                    @csrf 

                    <label class="mt-2  block text-sm">Nombre</label>
                    <input name="name" class="border border-gray-500 rounded-sm block w-full" value="{{ old('name') }}">

                    <label class="mt-2  block text-sm">R.I.F.</label>
                    <input name="dni" class="border border-gray-500 rounded-sm block w-full" value="{{ old('dni') }}">

                    <label class="mt-2  block text-sm">Dirección Fiscal</label>
                    <textarea class="border border-gray-500 rounded-sm block w-full" name="fiscal_address">{{ old('fiscal_address') }}</textarea>
                    
                    <label class="mt-2  block text-sm">Dirección</label>
                    <textarea class="border border-gray-500 rounded-sm block w-full" name="address">{{ old('address') }}</textarea>
                    
                    <label class="mt-2  block text-sm">Zona postal</label>
                    <input name="postal_zone" class="border border-gray-500 rounded-sm block w-full" value="{{ old('postal_zone') }}">

                    <label class="mt-2  block text-sm">Correo electronico</label>
                    <input name="email" class="border border-gray-500 rounded-sm block w-full" value="{{ old('email') }}">

                    <label class="mt-2  block text-sm">Correo electronico 2</label>
                    <input name="alt_email" class="border border-gray-500 rounded-sm block w-full" value="{{ old('alt_email') }}">

                    <label class="mt-2  block text-sm">Calle</label>
                    <input name="street" class="border border-gray-500 rounded-sm block w-full" value="{{ old('street') }}">

                    <label class="mt-2  block text-sm">Edificio</label>
                    <select class="g7_select" name="building">                
                        <option value="Edificio" {{ old('building') == 'Edificio' ? 'selected' : ''  }}>Edificio</option>
                        <option value="Local" {{ old('building') == 'Local' ? 'selected' : ''  }}>Local</option>
                        <option value="Casa" {{ old('building') == 'Casa' ? 'selected' : ''  }}>Casa</option>                       
                    </select>

                    <input type="hidden" name="status" value="Registrado">

                    <label class="mt-2  block text-sm">Numero edificio</label>
                    <input name="building_number" class="border border-gray-500 rounded-sm block w-full" value="{{ old('building_number') }}">
  

                    <label class="mt-2  block text-sm">Telefono</label>
                    <input name="phone" class="border border-gray-500 rounded-sm block w-full" value="{{ old('phone') }}">

                    <label class="mt-2  block text-sm">Telefono 2</label>
                    <input name="alt_phone" class="border border-gray-500 rounded-sm block w-full" value="{{ old('alt_phone') }}">

                    <label class="mt-2  block text-sm">Telefono celular</label>
                    <input name="celphone" class="border border-gray-500 rounded-sm block w-full" value="{{ old('celphone') }}">


                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.state_name') }})</label>
                    <select-state @state-selected="setStateId($event)" :old="'{{ old('state_id') }}'" 
                        class="g7_select"></select-state>
                        
                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.sector_name') }})</label>
                    <select-sector :state_id="state_id" 
                                :old="'{{ old('sector_id') }}'"
                                class="g7_select"></select-sector>

                    <button type="submit" class="btn mt-4">Registrar</button>

                </form>         
                
            </div>
        </div>
    </div>
</div>

@endsection