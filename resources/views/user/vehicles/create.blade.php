@extends('layouts.app')

@section('title')Publicar vehículo | @endsection 

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap justify-center">
        <div class=" w-full sm:w-3/4  lg:w-1/2 xl:w-2/5 border border-gray-500  rounded overflow-hidden shadow-md m-4">
            <div class=" flex items-center px-4 justify-between border-b border-gray-500">
                <div class="flex items-center ">
                    <svg class="fill-current h-4 w-4"  viewBox="0 0 20 20"><path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/></svg>
                    <h1 class="ml-3">Publicar vehículo</h1>
                </div>                    
                <div>
                    <a class="link" href="/user/vehicles">Cancelar</a>                          
                </div>        
            </div>
            <div class="  p-4">     
                
                <div class="text-red-700">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                <form method="POST" action="{{ route('user.vehicles.store') }}" class="w-full">
                    @csrf 

                    <label class="mt-1 block text-sm">Tipo de vehículo</label>
                    <select-vehicle-type @type-selected="setTypeId($event)" :old="'{{ old('vehicle_type_id') }}'" ></select-vehicle-type>
                
                    <label class="mt-2  block text-sm">Marca</label>
                    <select-brand @brand-selected="setBrandId($event)" :old="'{{ old('brand_id') }}'"></select-brand>

                    <label v-if="brand_id != ''" class="mt-2  block text-sm">Modelo</label>
                    <select-model v-if="brand_id != ''" :brand_id="brand_id" :old="'{{ old('model_id') }}'"></select-model>

                    <label class="mt-2  block text-sm">Color</label>
                    <select-color :old="'{{ old('color_id') }}'"></select-color>

                    <input type="hidden" name="status" value="Publicado">

                    <label v-if="type_id==3" class="mt-2  block text-sm">Cilindrada</label>
                    <select-displacement v-if="type_id==3" :old="'{{ old('displacement_id') }}'"></select-displacement>
                    
                    <div v-if="type_id != 3">
                        <label class="mt-2  block text-sm">Combustible</label>
                        <select-fuel :old="'{{ old('fuel') }}'"></select-fuel>
                    </div>
                    <label class="mt-2  block text-sm">Transmisión</label>
                    <select-transmission :old="'{{ old('transmission') }}'"></select-transmission>

                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.state_name') }})</label>
                    <select-state @state-selected="setStateId($event)" :old="'{{ old('state_id') }}'" 
                        class="g7_select"></select-state>
                        
                    <label class="mt-2  block text-sm">Ubicación ({{ setting('site.sector_name') }})</label>
                    <select-sector :state_id="state_id" 
                                :old="'{{ old('sector_id') }}'"
                                class="g7_select"></select-sector>
                
                    <label class="mt-2  block text-sm">Descripción</label>
                    <textarea class="border border-gray-500 rounded-sm block w-full" name="description">{{ old('description') }}</textarea>
                    
                    <label class="mt-2  block text-sm">Año</label>
                    <input name="year" class="border border-gray-500 rounded-sm block w-full" value="{{ old('year') }}">

                    <label class="mt-2  block text-sm">Kilometraje ({{ setting('site.distance_symbol') }})</label>
                    <input name="mileage" class="border border-gray-500 rounded-sm block w-full" value="{{ old('mileage') }}">

                    <label class="mt-2  block text-sm">Precio ({{ setting('site.currency') }})</label>
                    <input name="price" class="border border-gray-500 rounded-sm block w-full" value="{{ old('price') }}">

                    <div v-if="type_id != 3">
                        <label>Puertas</label>
                        <select class="g7_select" name="doors">                        
                            <option value="2" {{ old('doors') == 2 ? 'selected' : '' }}>2</option>
                            <option value="3" {{ old('doors') == 3 ? 'selected' : '' }}>3</option>
                            <option value="4" {{ old('doors') == 4 ? 'selected' : '' }}>4</option>
                            <option value="5" {{ old('doors') == 5 ? 'selected' : '' }}>5</option>
                        </select>
                    </div>
                    

                    <button type="submit" class="btn mt-4">Publicar</button>

                </form>         
                
            </div>
        </div>
    </div>
</div>

@endsection