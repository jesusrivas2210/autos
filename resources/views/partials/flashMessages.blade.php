@if (session('error'))
<div>
    <div class="m-2 bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
        <p>{{ session('error') }}</p>
    </div>
</div>
@endif
@if (session('warning'))
<div>
    <div class="m-2 bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">
        <p>{{ session('warning') }}</p>
    </div>
</div>
@endif
@if (session('success'))
<div>
    <div class="m-2 bg-green-100 border-l-4 border-green-500 text-green-700 p-4" role="alert">
        <p>{{ session('success') }}</p>
    </div>
</div>
@endif
@if (session('info'))
<div>
    <div class="m-2 bg-blue-100 border-l-4 border-blue-500 text-blue-700 p-4" role="alert">
        <p>{{ session('info') }}</p>
    </div>
</div>
@endif