<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="description here">
    <meta name="keywords" content="keywords,here">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title'){{ config('app.name', 'Garage7') }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> 
</head>

<body class="bg-white font-sans text-gray-900 leading-normal tracking-normal">
  <div id="app">

    <navbar :user="{{ Auth::check() ? Auth::user() : '-1' }}" :avatar_url="image_url">
    </navbar>
      
	  @include('partials.flashMessages')
    
    <main>
      @yield('content')
    </main>
  
  </div>

</body>
</html>