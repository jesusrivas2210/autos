<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\Models\Media;
use App\Models\Profile;
use App\Models\Garage;
use App\Models\Vehicle;
use App\Models\Message;
use App\Models\Transaction;
use \TCG\Voyager\Models\User as VoyagerUser;

class User extends VoyagerUser implements HasMedia, MustVerifyEmail
{
    use Notifiable, HasMediaTrait, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('avatar')->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('avatar')
              ->width(100)
              ->height(100);
    }

    public function avatarUrl()
    {
        if ($this->getMedia('avatar')->first() != null)
        {
            return $this->getMedia('avatar')->first()->getUrl('avatar');
        }                
        
        return '/img/avatar.png';        
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function garages()
    {
        return $this->hasMany(Garage::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

 
}
