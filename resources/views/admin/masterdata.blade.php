@extends('layouts.admin')

@section('title')Datos maestros - @endsection

@section('content')
<h1>Datos maestros</h1>

<div class="flex flex-wrap">
    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm2-2.25a8 8 0 0 0 4-2.46V9a2 2 0 0 1-2-2V3.07a7.95 7.95 0 0 0-3-1V3a2 2 0 0 1-2 2v1a2 2 0 0 1-2 2v2h3a2 2 0 0 1 2 2v5.75zm-4 0V15a2 2 0 0 1-2-2v-1h-.5A1.5 1.5 0 0 1 4 10.5V8H2.25A8.01 8.01 0 0 0 8 17.75z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Paises</h5>
                <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20S3 10.87 3 7a7 7 0 1 1 14 0c0 3.87-7 13-7 13zm0-11a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Estados</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 8-8 4-2 8z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Municipios</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2 14v-3H1a1 1 0 0 1-1-1 1 1 0 0 1 1-1h1l4-7h8l4 7h1a1 1 0 0 1 1 1 1 1 0 0 1-1 1h-1v6a1 1 0 0 1-1 1h-1a1 1 0 0 1-1-1v-1H5v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-3zm13.86-5L13 4H7L4.14 9h11.72zM5.5 14a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm9 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Tipos de vehículo</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Marcas</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M19 11a7.5 7.5 0 0 1-3.5 5.94L10 20l-5.5-3.06A7.5 7.5 0 0 1 1 11V3c3.38 0 6.5-1.12 9-3 2.5 1.89 5.62 3 9 3v8zm-9 1.08l2.92 2.04-1.03-3.41 2.84-2.15-3.56-.08L10 5.12 8.83 8.48l-3.56.08L8.1 10.7l-1.03 3.4L10 12.09z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Modelos</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9 20v-1.7l.01-.24L15.07 12h2.94c1.1 0 1.99.89 1.99 2v4a2 2 0 0 1-2 2H9zm0-3.34V5.34l2.08-2.07a1.99 1.99 0 0 1 2.82 0l2.83 2.83a2 2 0 0 1 0 2.82L9 16.66zM0 1.99C0 .9.89 0 2 0h4a2 2 0 0 1 2 2v16a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zM4 17a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Colores</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm-5.6-4.29a9.95 9.95 0 0 1 11.2 0 8 8 0 1 0-11.2 0zm6.12-7.64l3.02-3.02 1.41 1.41-3.02 3.02a2 2 0 1 1-1.41-1.41z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Cilindradas</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

    <div class="w-full md:w-1/2 xl:w-1/3 p-3">
        <!--Metric Card-->
        <a href="{{ route('countries.index') }}" class="">
        <div class="bg-white border rounded shadow p-2 hover:bg-gray-100">
            <div class="flex flex-row items-center">
                <div class="flex-shrink pr-4">
                    <div class="rounded p-3 bg-green-600"><svg class="h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M18 6V4H2v2h16zm0 4H2v6h16v-6zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm4 8h4v2H4v-2z"/></svg></div>
                </div>
                <div class="flex-1 text-right md:text-center">
                    <h5 class="font-bold uppercase text-gray-500">Métodos de pago</h5>
                    <h3 class="font-bold text-3xl">1</h3>
                </div>
            </div>
        </div>
        </a>
    </div>

</div>
@endsection