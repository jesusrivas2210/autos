<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreModel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'models.store') {
            return [
                'name' => 'required|unique:models|max:64',
                'brand_id' => 'required|integer',
            ];
        }

        return [
            'brand_id' => 'required|integer',
            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('models')->ignore($this->route('model'))]
        ];
    }
}
