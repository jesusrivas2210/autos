<?php

Route::get('/', 'HomepageController@homepage')->name('homepage');

/* Modulo de autenticacion */
Auth::routes(['verify' => true]);
Route::get('/changePassword','Auth\ChangePasswordController@showChangePasswordForm');
Route::post('/changePassword','Auth\ChangePasswordController@changePassword')->name('changePassword');

/* Modulo de administracion */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

/* Modulo Perfil de usuario */
Route::get('/user/profile', 'UserProfileController@show')->name('user.profile');
Route::get('/user/profile/create', 'UserProfileController@create')->name('user.profile.create');
Route::post('/user/profile', 'UserProfileController@store')->name('user.profile.store');
Route::get('/user/profile/edit', 'UserProfileController@edit')->name('user.profile.edit');
Route::patch('/user/profile', 'UserProfileController@update')->name('user.profile.update');

/* Modulo de vehiculos */
Route::get('/vehicles', 'VehiclesController@index')->name('vehicles');
Route::get('/vehicles/{vehicle}', 'VehiclesController@show')->name('vehicles.show');
Route::get('/user/vehicles', 'UserVehiclesController@index')->name('user.vehicles');
Route::get('/user/vehicles/create', 'UserVehiclesController@create')->name('user.vehicles.create');
Route::get('/user/vehicles/{vehicle}/edit', 'UserVehiclesController@edit')->name('user.vehicles.edit');
Route::post('/user/vehicles', 'UserVehiclesController@store')->name('user.vehicles.store');
Route::patch('/user/vehicles/{vehicle}/update', 'UserVehiclesController@update')->name('user.vehicles.update');

/* Modulo de concesionarios */
Route::get('/user/garages', 'UserGaragesController@index')->name('user.garages');
Route::get('/user/garages/create', 'UserGaragesController@create')->name('user.garages.create');
Route::post('/user/garages/store', 'UserGaragesController@store')->name('user.garages.store');
Route::get('/user/garages/{garage}/edit', 'UserGaragesController@edit')->name('user.garages.edit');
Route::patch('/user/garages/{garage}/update', 'UserGaragesController@update')->name('user.garages.update');

/** Modulo de preguntas al vendedor */
Route::post('/vehicles/{vehicle}/messages', 'MessagesController@store')->name('messages.store');
Route::patch('/messages/{message}', 'MessagesController@update')->name('messages.update');