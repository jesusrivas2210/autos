<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePhoto;

class ProfileImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function avatar(StorePhoto $request)
    {
        $user = request()->user();
        $user->addMediaFromRequest('photo')->toMediaCollection('avatar');
        $url = request()->user()->avatarUrl();

        return response()->json(['avatar_url' => $url]);
    }

    public function avatarUrl()
    {
        $url = request()->user()->avatarUrl();
        
        return response()->json(['avatar_url' => $url]);
    }
}
