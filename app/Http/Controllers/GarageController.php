<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Garage;
use App\Http\Resources\GarageCollection;
use App\Http\Resources\Garage as GarageResource;

class GarageController extends Controller
{
    public function index()
    {
        $garages = Garage::paginate();
        
        return new GarageCollection($garages);
    }

    public function show($garage)
    {
        $garage = Garage::with('user', 'country', 'state', 'sector')->find($garage);

        return new GarageResource($garage);
    }
}
