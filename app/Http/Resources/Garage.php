<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Garage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'id_number' => $this->id_number,
            'fiscal_address' => $this->fiscal_address,
            'address' => $this->address,
            'phone' => $this->phone,
            'postal_zone' => $this->postal_zone,
            'celphone' => $this->celphone,
            'alt_phone' => $this->alt_phone,
            'alt_email' => $this->alt_email,
            'email' => $this->email,
            'country_id' => $this->country_id,
            'state_id' => $this->state_id,
            'sector_id' => $this->sector_id,
            'street' => $this->street,
            'building' => $this->building,
            'building_number' => $this->id_number,
            'user_id' => $this->user_id,
            'country' => $this->country->name,
            'state' => $this->state->name,
            'sector' => $this->sector->name,
            
            
        ];
    }
}
