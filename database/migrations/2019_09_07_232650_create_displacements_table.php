<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('displacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 64)->unique()->comment('nombre de la cilindrada');
            $table->boolean('is_active')->comment('si esta activo se puede seleccionar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('displacements');
    }
}
