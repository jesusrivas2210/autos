<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Garage extends Model
{
    protected $guarded = ['state_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
