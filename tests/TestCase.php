<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected function we() 
    {
    	$this->withoutExceptionHandling();
    }

    protected function createUser($role = 'usuario')
    {
       
        $user = factory(User::class)->create();
        

        return $user;
    }

    protected function createAdmin()
    {
        return $this->createUser('administrador');
    }

    protected function createOperador()
    {
        return $this->createUser('operador');
    }

    protected function createRole($role = 'usuario')
    {
       // Role::create(['name' => $role]);
    }

    protected function assertValidationsErrors($fields, $route, $method = 'get', $table = null,
      $user = null)
    {
        foreach($fields as $data) {
            $keys = array_keys($data);
            $response = null;
            if ($user !== null) {
                $response = $this->actingAs($user)->$method($route, $data);
            }
            else{
                $response = $this->$method($route, $data);
            }
            
            $response->assertSessionHasErrors($keys[0]);
            if ($table !== null) {
                $this->assertDatabaseMissing($table, $data);
            }
            
        }
    }
}