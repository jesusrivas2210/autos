<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();            
            $table->bigInteger('model_id')->unsigned();
            $table->bigInteger('sector_id')->unsigned();
            $table->bigInteger('color_id')->unsigned();
            $table->bigInteger('vehicle_type_id')->unsigned();
            $table->bigInteger('displacement_id')->unsigned()->nullable();
            $table->enum('fuel', ['Diésel', 'Gasolina', 'Gas', 'Eléctrico'])->nullable();
            $table->text('description');
            $table->bigInteger('price')->unsigned(); // in cents
            $table->tinyInteger('doors')->nullable();
            $table->string('year', 4);
            $table->string('mileage'); 
            $table->enum('transmission', ['Manual', 'Automática'])->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
            $table->string('status'); 

            $table->foreign('sector_id')->references('id')->on('sectors');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('color_id')->references('id')->on('colors');
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types');
            $table->foreign('displacement_id')->references('id')->on('displacements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
