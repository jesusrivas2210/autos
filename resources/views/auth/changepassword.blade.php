@extends('layouts.app')

@section('title'){{ __('messages.change_password') }} -@endsection

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap justify-center  mt-4">
        <div class="w-full max-w-sm">
            <div class="flex flex-col break-words bg-white border border-2 rounded shadow-md">

                <div class="font-semibold  text-gray-700 py-3 px-6 mb-0 text-center mt-3">
                    {{ __('messages.change_password') }}
                </div>

                <form class="w-full p-6" method="POST" action="{{ route('changePassword') }}">
                    @csrf

                    <div class="flex flex-wrap mb-6">
                        <label for="current_password" class="block text-gray-700 text-sm font-bold mb-2">
                            {{ __('messages.current_password') }}:
                        </label>

                        <input id="current_password" type="password" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline{{ $errors->has('current_password') ? ' border-red-500' : '' }}" name="current_password" required>

                        @if ($errors->has('current_password'))
                            <p class="text-red-500 text-xs italic mt-4">
                                {{ $errors->first('current_password') }}
                            </p>
                        @endif
                    </div>     

                    <div class="flex flex-wrap mb-6">
                        <label for="password" class="block text-gray-700 text-sm font-bold mb-2">
                            {{ __('messages.new_password') }}:
                        </label>

                        <input id="password" type="password" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline{{ $errors->has('password') ? ' border-red-500' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <p class="text-red-500 text-xs italic mt-4">
                                {{ $errors->first('password') }}
                            </p>
                        @endif
                    </div>

                    <div class="flex flex-wrap mb-6">
                        <label for="password_confirmation" class="block text-gray-700 text-sm font-bold mb-2">
                            {{ __('messages.confirm_new_password') }}:
                        </label>

                        <input id="password_confirmation" type="password" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="password_confirmation" required>
                    </div>

                    <div class="flex flex-wrap">
                        <button type="submit" class="inline-block align-middle text-center select-none border font-bold whitespace-no-wrap py-2 px-4 rounded text-base leading-normal no-underline text-gray-100 bg-blue-800 hover:bg-blue-700">
                            {{ __('messages.change_password') }}
                        </button>

                        
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection