<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreSector extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'sectors.store') {
            return [
                'name' => 'required|unique:sectors|max:64',
                'state_id' => 'required|integer',
            ];
        }

        return [
            'state_id' => 'required|integer',
            'name' => [ 'required', 
                         'max:64',
                         Rule::unique('sectors')->ignore($this->route('sector'))]
        ];
    }
}
