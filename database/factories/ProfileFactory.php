<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'dni' => '123456789',
        'phone' => '123456789',
        'address' => 'Calle principal',
        'city' => $faker->city,
        //'sector_id' => factory(App\Models\Sector::class)->create()->id, 
        //'user_id' => factory(App\User::class)->create()->id,
    ];
});
