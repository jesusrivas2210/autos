<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Displacement;
use Faker\Generator as Faker;

$factory->define(Displacement::class, function (Faker $faker) {
    return [
        'name' => $this->faker->unique()->numerify('### CC'),
        'is_active' => true
    ];
});
