<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        /*factory(App\User::class)->create([
            'email' => 'admin@correo.com',
            'name' => 'Administrador'
        ]);

        factory(App\User::class)->create([
            'email' => 'operador@correo.com',
            'name' => 'Operador'
        ]);

        factory(App\User::class)->create([
            'email' => 'usuario@correo.com',
            'name' => 'Jesus'
        ]);*/

        /* Marcas */
        /*factory(App\Models\Brand::class)->create(['name'=>'Susuki']);
        factory(App\Models\Brand::class)->create(['name'=>'Ford']);
        factory(App\Models\Brand::class)->create(['name'=>'Toyota']);
        factory(App\Models\Brand::class)->create(['name'=>'Fiat']);
        factory(App\Models\Brand::class)->create(['name'=>'Chevrolet']);
        factory(App\Models\Brand::class)->create(['name'=>'Audi']);
        factory(App\Models\Brand::class)->create(['name'=>'Dodge']);
        /* Modelos */
       /* factory(App\Models\Model::class)->create(['name'=>'Aerostar','brand_id'=>2]);
        factory(App\Models\Model::class)->create(['name'=>'EcoSport','brand_id'=>2]);
        factory(App\Models\Model::class)->create(['name'=>'4Runner','brand_id'=>3]);
        factory(App\Models\Model::class)->create(['name'=>'Autana','brand_id'=>3]);
        factory(App\Models\Model::class)->create(['name'=>'Adventure','brand_id'=>4]);
        factory(App\Models\Model::class)->create(['name'=>'Palio','brand_id'=>4]);
        factory(App\Models\Model::class)->create(['name'=>'Corsa','brand_id'=>5]);
        factory(App\Models\Model::class)->create(['name'=>'Astra','brand_id'=>5]);
        factory(App\Models\Model::class)->create(['name'=>'A1','brand_id'=>6]);
        factory(App\Models\Model::class)->create(['name'=>'A3','brand_id'=>6]);
        factory(App\Models\Model::class)->create(['name'=>'Dart','brand_id'=>7]);
        factory(App\Models\Model::class)->create(['name'=>'Neon','brand_id'=>7]);
        /* Colores */
        /*factory(App\Models\Color::class)->create(['name'=>'Amarillo']);
        factory(App\Models\Color::class)->create(['name'=>'Azul']);
        factory(App\Models\Color::class)->create(['name'=>'Gris']);
        factory(App\Models\Color::class)->create(['name'=>'Verde']);
        /* Cilindradas */
        /*factory(App\Models\Displacement::class)->create(['name'=> '100 cc']);
        factory(App\Models\Displacement::class)->create(['name'=> '120 cc']);
        factory(App\Models\Displacement::class)->create(['name'=> '150 cc']);
            /** Tipos de vehiculo */
       /* factory(App\Models\VehicleType::class)->create(['name'=>'Carro']);
        factory(App\Models\VehicleType::class)->create(['name'=>'Moto']);
        factory(App\Models\VehicleType::class)->create(['name'=>'Camion']);
        factory(App\Models\VehicleType::class)->create(['name'=>'Camioneta']);
        /** Estados */
        /*factory(App\Models\State::class)->create(['name'=>'Miranda']);
        factory(App\Models\State::class)->create(['name'=>'Lara']);
        factory(App\Models\State::class)->create(['name'=>'Zulia']);
        factory(App\Models\State::class)->create(['name'=>'Sucre']);
        /** Municipios */
        /*factory(App\Models\Sector::class,3)->create(['state_id'=>2]);
        factory(App\Models\Sector::class,3)->create(['state_id'=>3]);
        factory(App\Models\Sector::class,3)->create(['state_id'=>4]);
        factory(App\Models\Sector::class,3)->create(['state_id'=>5]);
        /** Metodos pago */
       /* factory(App\Models\PaymentMethod::class)->create(['name'=>'Cash']);
        factory(App\Models\PaymentMethod::class)->create(['name'=>'Deposit']);

        /** usuarios */
        /*factory(App\Models\Profile::class, 5)->create(['sector_id'=>2]);
        factory(App\Models\Profile::class, 5)->create(['sector_id'=>4]);
        factory(App\Models\Profile::class, 5)->create(['sector_id'=>5]);

        /** garages */
       /* factory(App\Models\Garage::class,2)->create(['sector_id'=>2]);
        factory(App\Models\Garage::class,2)->create(['sector_id'=>4]);
        factory(App\Models\Garage::class,2)->create(['sector_id'=>5]);

        /** vehicluos */
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 2,
            'vehicle_type_id' =>2,
            'user_id' => 4,
            'model_id' => 2,
            'color_id' => 2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 2,
            'vehicle_type_id' =>2,
            'user_id' => 5,
            'model_id' => 3,
            'color_id' => 3,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 2,
            'vehicle_type_id' =>3,
            'user_id' => 6,
            'model_id' => 3,
            'color_id' => 3,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 2,
            'vehicle_type_id' =>3,
            'user_id' => 7,
            'model_id' => 3,
            'color_id' => 4,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 2,
            'vehicle_type_id' =>2,
            'user_id' => 8,
            'model_id' => 6,
            'color_id' => 1,
            'displacement_id' => null,
        ]);

        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 4,
            'vehicle_type_id' =>2,
            'user_id' => 9,
            'model_id' => 8,
            'color_id' => 2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 4,
            'vehicle_type_id' =>4,
            'user_id' => 10,
            'model_id' => 9,
            'color_id' => 2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 4,
            'vehicle_type_id' =>3,
            'user_id' => 11,
            'model_id' => 10,
            'color_id' => 1,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 4,
            'vehicle_type_id' =>3,
            'user_id' => 12,
            'model_id' => 2,
            'color_id' =>2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 4,
            'vehicle_type_id' =>2,
            'user_id' => 13,
            'model_id' => 6,
            'color_id' => 1,
            'displacement_id' => null,
        ]);

        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 5,
            'vehicle_type_id' =>2,
            'user_id' => 14,
            'model_id' => 7,
            'color_id' => 2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 5,
            'vehicle_type_id' =>3,
            'user_id' => 15,
            'model_id' => 6,
            'color_id' => 3,
            'displacement_id' => 2,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 5,
            'vehicle_type_id' =>3,
            'user_id' => 16,
            'model_id' => 5,
            'color_id' => 2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 5,
            'vehicle_type_id' =>2,
            'user_id' => 17,
            'model_id' => 2,
            'color_id' =>2,
            'displacement_id' => null,
        ]);
        factory(App\Models\Vehicle::class,30)->create([
            'sector_id' => 5,
            'vehicle_type_id' =>2,
            'user_id' => 18,
            'model_id' =>10,
            'color_id' => 3,
            'displacement_id' => null,
        ]);

    }
}
