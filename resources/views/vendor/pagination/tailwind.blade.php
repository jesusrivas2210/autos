@if ($paginator->hasPages())
    <div class="flex items-center text-sm">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <span class="rounded-l rounded-sm border border-gray-500 px-2 px-1 cursor-not-allowed no-underline">&laquo;</span>
        @else
            <a
                class="rounded-l rounded-sm border-t border-b border-l border-gray-500 px-2 px-1 text-gray-800 hover:bg-blue-200 no-underline"
                href="{{ $paginator->previousPageUrl() }}"
                rel="prev"
            >
                &laquo;
            </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span class="border-t border-b border-l bor border-gray-500 px-2 px-1 cursor-not-allowed no-underline">{{ $element }}</span>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <span class="border-t border-b border-l border-gray-500 px-2 px-1 bg-blue-200 no-underline">{{ $page }}</span>
                    @else
                        <a class="border-t border-b border-l border-gray-500 px-2 px-1 hover:bg-blue-200 text-gray-800 no-underline" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="rounded-r rounded-sm border border-gray-500 px-2 px-1 hover:bg-blue-200 text-gray-800 no-underline" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a>
        @else
            <span class="rounded-r rounded-sm border border-gray-500 px-2 px-1 hover:bg-blue-200 text-gray-800 no-underline cursor-not-allowed">&raquo;</span>
        @endif
    </div>
@endif