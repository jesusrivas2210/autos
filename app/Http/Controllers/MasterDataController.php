<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Color;
use App\Models\Model;
use App\Models\State;
use App\Models\Sector;
use App\Models\VehicleType;
use App\Models\Displacement;
use Illuminate\Http\Request;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\ColorCollection;
use App\Http\Resources\ModelCollection;
use App\Http\Resources\StateCollection;
use App\Http\Resources\SectorCollection;
use App\Http\Resources\VehicleTypeCollection;
use App\Http\Resources\DisplacementCollection;

class MasterDataController extends Controller
{
    public function brands()
    {
        return BrandCollection::make(Brand::where('is_active', 1)->get());
    }

    public function colors()
    {
        return ColorCollection::make(Color::where('is_active', 1)->get());
    }

    public function displacements()
    {
        return DisplacementCollection::make(Displacement::where('is_active', 1)->get());
    }

    public function states()
    {
        return StateCollection::make(State::where('is_active', 1)->get());
    }

    public function sectors($state)
    {
        $sectors = Sector::where('is_active', 1)->where('state_id', $state)->get();

        return SectorCollection::make($sectors);
    }

    public function vehicleTypes()
    {
        return VehicleTypeCollection::make(
            VehicleType::where('is_active', request()->is_active)
            ->get());
    }

    public function models($brand)
    {
        $models = Model::where('is_active', 1)->where('brand_id', $brand)->get();

        return ModelCollection::make($models);
    }
}
