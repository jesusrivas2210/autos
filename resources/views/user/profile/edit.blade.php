@extends('layouts.app')

@section('title')Editar Mi Cuenta | @endsection

@section('content')
<div class="container mx-auto">
    <div class="flex flex-wrap justify-center">
        <div class=" w-full sm:w-3/4  lg:w-1/2 xl:w-2/5 border border-gray-500  rounded overflow-hidden shadow-md m-4">
            <div class=" flex items-center px-4 justify-between border-b border-gray-500">
                <div class="flex items-center ">
                    <svg class="fill-current h-4 w-4"  viewBox="0 0 20 20"><path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/></svg>
                    <h1 class="ml-3">Editar Mi Cuenta</h1>
                </div>                    
                <div>
                    <a class="link" href="/user/profile">Cancelar</a>                          
                </div>        
            </div>
            <div class="sm:flex  p-4">
                <div class="mb-4 sm:mb-0">
                    <profile-photo @avatarchanged="avatar()" :avatar_url="image_url"></profile-photo>
                </div>
                <div class="sm:ml-4">
                    <div class="text-red-700">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    <form method="POST" action="{{ route('user.profile.update') }}">
                        @csrf 
                        @method('PATCH')
                    <h2>Datos Personales</h2>
                    <div class="text-sm">
                        <div>
                            <label class="font-semibold">Nombres y Apellidos:</label> 
                            {{ $user->name }} {{ $user->lastname }}
                        </div>
                        
                        <div class="mt-2">
                            <label class="font-semibold">Correo Electrónico:</label> 
                            {{ $user->email }}
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.dni_type') }}:</label>
                            <input name="dni" 
                                    value="{{ old('dni', $profile->dni) }}" 
                                    class="block border border-gray-500">        
                        </div>

                        <div class="mt-2">
                            <label class="font-semibold">Teléfono:</label>
                            <input name="phone" 
                                    value="{{ old('phone', $profile->phone) }}"  
                                    class="block border border-gray-500">       
                        </div>
                    </div>
                    <h2 class="mt-4">Datos de Ubicación</h2>
                    <div class="text-sm">
                        <div>
                            <label class="font-semibold">Dirección:</label>
                            <textarea name="address"   
                                class="block border border-gray-500">{{ old('address', $profile->address) }}</textarea>        
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">Ciudad:</label>
                            <input name="city"  value="{{ old('city', $profile->city)  }}"
                             class="block border border-gray-500">                
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.state_name') }}:</label>
                            <select-state @state-selected="setStateId($event)" :old="'{{ old('state_id', $profile->sector->state_id) }}'" 
                                class="block  p-1 border border-gray-500 bg-white"></select-state>        
                        </div>
                        <div class="mt-2">
                            <label class="font-semibold">{{ setting('site.sector_name') }}:</label>
                            <select-sector :state_id="state_id" 
                                :old="'{{ old('sector_id', $profile->sector_id) }}'"
                                class="block p-1 border border-gray-500 bg-white"></select-sector>
                        </div>
                    </div>
                    <div class="mt-2">
                        <button type="submit" class="btn">Guardar</button>
                    </div>
                    </form>         
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection