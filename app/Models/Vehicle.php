<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Vehicle extends Eloquent implements HasMedia
{

    use HasMediaTrait;

    protected $guarded = ['brand_id', 'state_id'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('photos');
    }

    public function registerMediaConversions(Media $media = null)
    {
        /*$this->addMediaConversion('miniatura')
              ->width(100)
              ->height(100);*/
    }

    public function getMediaUrls()
    {
        $urls = [];
        if ($this->getMedia('photos') != null)
        {
            $urls = $this->getMedia('photos')->map(function ($media) {
                return ['url' => $media->getUrl(), 'id' => $media->id ];
            });
        }
        return $urls;
    }

    public function firstMediaUrl()
    {
        if ($this->hasMedia('photos'))
        {
            return $this->getFirstMediaUrl('photos');
        }

        return '/img/Automovil.jpg';
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function model()
    {
        return $this->belongsTo(Model::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function vehicleType()
    {
        return $this->belongsTo(VehicleType::class);
    }
    
    public function displacement()
    {
        return $this->belongsTo(Displacement::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    // scopes
    public function scopeModel($query, $model)
    {
        if ($model) {
            return $query->whereHas('model', function ($query) use ($model) {
                $query->where('name', 'like', "%$model%")
                    ->orWhereHas('brand', function ($query) use ($model) {
                        $query->where('name', 'like', "%$model%");
                    })
                ;
            });
        }
            
    }

    public function scopeOrder($query, $order)
    {
        if ($order) {
            if ($order == 'price_desc') {
                return $query->orderBy('price', 'desc');
            } else if ($order == 'price_asc') {
                return $query->orderBy('price', 'asc');
            } else if ($order == 'date_desc') {
                return $query->orderBy('created_at', 'desc');
            } else {
                return $query->orderBy('created_at', 'asc');
            }            
        }
        else {
            return $query->orderBy('created_at', 'desc');
        }
            
    }

    public function scopeType($query, $type)
    {
        if ($type)
            return $query->where('vehicle_type_id', $type);
    }

    public function scopeBrand($query, $brand)
    {
        if ($brand) {
            return $query->whereHas('model', function ($query) use ($brand) {
                $query->where('brand_id', $brand);
            });
        }            
    }

    public function scopeModelId($query, $id)
    {
        if ($id)
            return $query->where('model_id', $id);
    }

    public function scopeFuel($query, $fuel)
    {
        if ($fuel)
            return $query->where('fuel', 'like', "%$fuel%");
    }

    public function scopeYearTo($query, $year)
    {
        if ($year)
            return $query->where('year', '<=', $year);
    }

    public function scopeYearFrom($query, $year)
    {
        if ($year)
            return $query->where('year', '>=', $year);
    }

    public function scopePriceTo($query, $price)
    {
        if ($price)
            return $query->where('price', '<=', $price * 100);
    }

    public function scopePriceFrom($query, $price)
    {
        if ($price)
            return $query->where('price', '>=', $price * 100);
    }

    public function scopeTransmission($query, $transmission)
    {
        if ($transmission)
            return $query->where('transmission', 'like', "%$transmission%");
    }

    public function scopeColor($query, $color)
    {
        if ($color)
            return $query->where('color_id', $color);
    }

    public function scopeSector($query, $sector)
    {
        if ($sector)
            return $query->where('sector_id', $sector);
    }

    public function scopeState($query, $state)
    {
        if ($state) {
            return $query->whereHas('sector', function ($query) use ($state) {
                $query->where('state_id', $state);
            });
        }            
    }

    public function scopeDisplacement($query, $displacement)
    {
        if ($displacement)
            return $query->where('displacement_id', $displacement);
    }
}
