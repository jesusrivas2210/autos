<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Garage;
use Faker\Generator as Faker;

$factory->define(Garage::class, function (Faker $faker) {
    return [
        'id_number' => 258,
        'name' => $faker->name,
        'fiscal_address' => $faker->address,
        'address' => $faker->address,
        'postal_zone' => '1220',
        'phone' => $faker->e164PhoneNumber,
        'celphone' => $faker->e164PhoneNumber,
        'alt_phone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'alt_email' => $faker->unique()->safeEmail,
       
        'user_id' => factory(App\User::class)->create()->id,
        'street' => $faker->address,
        'building' => 'Local',
        'building_number' => 1,
    ];
});
