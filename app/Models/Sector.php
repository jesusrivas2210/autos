<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $guarded = [];

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }

    public function garages()
    {
        return $this->hasMany(Garage::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }
}
