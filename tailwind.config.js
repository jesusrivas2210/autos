module.exports = {
  theme: {
    extend: {
      colors: {
        blueg7: '#00305c',
        orangeg7: '#f7a30a'
      },
    }
  },
  variants: {},
  plugins: []
}
