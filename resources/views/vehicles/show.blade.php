@extends('layouts.app')

@section('content')
<div class="container w-full mx-auto">
    
        
  
    <div class="my-2">
        <a class="inline link flex items-center" href="{{ url()->previous() }}"><svg class="h-4 w-4 fill-current "   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M7.05 9.293L6.343 10 12 15.657l1.414-1.414L9.172 10l4.242-4.243L12 4.343z"/></svg><span>Volver al listado</span></a>
    </div>
    <lightbox :images="{{ $vehicle->getMediaUrls() }}" ></lightbox>
    <div class="flex flex-wrap -mb-4">
        <div class="lg:w-1/2">
            <div class="m-2  bg-white rounded overflow-hidden border border-gray-500 inline-block text-gray-800">
               <div class="p-8">            
                    <h1 class="font-semibold text-lg">{{ $vehicle->model->brand->name }} {{ $vehicle->model->name }}</h1>
                    <span class="text-xs">Publicado {{ $vehicle->created_at->diffForHumans() }}</span>                        
                    <div class="mt-2">{{ $vehicle->year }}</div>
                    <div class="text-sm">{{ $vehicle->sector->name }}, {{ $vehicle->sector->state->name }}</div>
                    <div class="text-base  mt-4">{{ $vehicle->description }}</div>
                    <div class="text-sm mt-4">Tipo: {{ $vehicle->vehicleType->name }}</div>
                    <div class="text-sm">Color: {{ $vehicle->color->name }}</div>
                    <div class="text-sm">Transmisión: {{ $vehicle->transmission }}</div>
                    <div class="text-sm">Combustible: {{ $vehicle->fuel }}</div>
                    <div class="text-sm">Cilindrada: {{ $vehicle->displacement ? $vehicle->displacement->name : 'N/A' }}</div>
                    <div class="text-sm">Kilometraje: {{ $vehicle->mileage }} {{ setting('site.distance_symbol') }}</div>
                    <div class="text-sm">Puertas: {{ $vehicle->doors }}</div>
                    <div class="text-xl mt-8">Precio: <span class="text-base">{{ setting('site.currency') }}</span> {{number_format($vehicle->price/100, 2, 
                        setting('site.decimal_point'), setting('site.thousands_separator'))}}</div>

                    
                    
                
                </div>
            </div>
        </div>

        <div class="lg:w-1/2 ">
            <div class="m-2 w-full bg-white rounded overflow-hidden border border-gray-500 inline-block text-gray-800">
                <h2 class="p-2">Preguntas al vendedor</h2>

                <div class="my-2">
                    @foreach($vehicle->messages as $message)
                    <div class="p-2">
                        <span class="font-semibold block">Pregunta: {{ $message->question }}</span>
                        <span class="text-xs">{{ $message->created_at->diffForHumans() }} por {{ $message->user->name }}</span>
                        @if( auth()->id() == $vehicle->user_id )
                        @if($message->answer)
                        <div class="px-2">Respuesta: {{ $message->answer }}</div>
                        <span class="text-xs px-2">{{ $message->answered_at->diffForHumans() }} por {{ $message->vehicle->user->name }}</span>
                      
                        @else
                        <form method="POST" action="{{ route('messages.update', ['message' => $message->id]) }}">
                            @csrf 
                            @method('patch')
                            <input placeholder="Responde al comprador" name="question" class="w-full px-1 block border border-gray-500 rounded">
                            <button type="submit" class="mt-2 btn">Enviar</button>
                        </form>
                        @endif
                        @endif
                    </div>
                    @endforeach
                </div>
                @if( auth()->id() != $vehicle->user_id )
                <div class="mt-4 p-2">
                    <form method="POST" action="{{ route('messages.store', ['vehicle' => $vehicle->id]) }}">
                        @csrf 
                        <textarea placeholder="Pregunta al vendedor" name="question" class="w-full px-1 block border border-gray-500 rounded"></textarea>
                        <button type="submit" class="mt-2 btn">Enviar</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
    
</div>


@endsection