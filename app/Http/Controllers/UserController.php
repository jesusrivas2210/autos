<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;
use App\Http\Resources\User as UserResource;
use App\Mail\UserCreated;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate();

        return new UserCollection($users);
    }

    public function store(StoreUser $request)
    {
        $user = new User;
        $user->fill($request->all());
        $password = str_random(8);
        $user->password = Hash::make($password);
        $user->save();

        Mail::to($user)->queue(new UserCreated());

        return new UserResource($user);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function update(StoreUser $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['deleted' => true]);
    }
}
