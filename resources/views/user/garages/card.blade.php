
<div class="mb-2 flex bg-white rounded-lg  overflow-hidden border border-gray-500 block text-gray-800">
    <img class="p-1 w-24 h-24 sm:h-16 sm:w-16 object-cover object-center" src="{{ asset('img/Automovil.jpg') }}" >
    <div class="p-1 sm:flex items-center w-full">
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6">
        <a class="underline" href="{{ route('user.garages.edit', ['garage' => $garage->id]) }}">
                {{ $garage->name }} 
            </a>
            
        </div>
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6">{{ $garage->dni }}</div>
        <div class="hidden md:block md:w-1/5 lg:w-1/6">{{ $garage->sector->name }}, {{ $garage->sector->state->name }}</div>
        <div class="sm:w-1/4 md:w-1/5 lg:w-1/6" >Publicado {{ $garage->created_at->diffForHumans() }}</div>
        <div class="  hidden lg:block lg:w-1/6">{{ $garage->email }}
        </div>
    <div class="sm:w-1/4 md:w-1/5 lg:w-1/6">{{ $garage->status }}</div>
    </div>
</div> 

