@extends('layouts.app')

@section('title')Mis vehículos | @endsection 

@section('content')
<div class="container mx-auto">
    <div class="mx-1 sm:flex flex-wrap items-center justify-between mb-3">
        <h1 class="my-4 ">Mis vehículos</h1>
        <a class="py-1 px-3 hover:bg-green-600 text-sm rounded-lg bg-green-400 border border-green-600 text-white" href="{{ route('user.vehicles.create') }}">Publicar vehículo</a>    
    </div>
    <div class="mx-auto flex flex-wrap justify-center">
            
        <div class="w-full">            
            @foreach($vehicles as $vehicle)
            <div class="mx-1">
                @include('user.vehicles.card',['vehicle' => $vehicle])
            </div>            
            @endforeach            
        </div>         

        <div class="my-4 mx-1 ">
            {{ $vehicles->appends(request()->query())->links('vendor.pagination.tailwind') }}
        </div>
    </div>    
</div>
@endsection