/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('infinite', require('vue-infinite-loading'));

const app = new Vue({
	el: '#app',
	data: {		
		state_id: '',
		brand_id: '',
		image_url: '',
		type_id: '',
		is_filter_open: true,
	},
	created() {
		this.avatar()
	},	
	methods: {
		isFilterOpen() {
			this.is_filter_open = ! this.is_filter_open;
		},
		setStateId(id) {
			this.state_id = id;
		},
		setBrandId(id) {
			this.brand_id = id;
		},
		setTypeId(id) {
			this.type_id = id;
		},
		avatar() {			
			axios.get('/api/user/profile/avatar-url')
			.then(response => {
				if (response.data.avatar_url!="")
					this.image_url = response.data.avatar_url              
				})			    
		},
		submitFilter() {
			document.getElementById('filter-form').submit();
		}
	}
});