<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Profile;
use App\Models\Sector;
use App\Models\State;

class UserProfileTest extends TestCase
{
    /** @test */
    public function auth_user_can_visit_his_profile_page()
    {
        $user = $this->createUser();
        $state = factory(State::class)->create();
        $sector = factory(Sector::class)->create(['state_id' => $state->id]);
        $profile = factory(Profile::class)->make(['sector_id' => $sector->id]);
        $user->profile()->save($profile);

        $response = $this->actingAs($user)->get(route('user.profile'));

        $response->assertStatus(200);
        $response->assertViewIs('user.profile.show');
        $response->assertViewHas(['profile', 'user']);
        $response->assertSeeText($user->name);
        $response->assertSeeText($user->lastname);
        $response->assertSeeText($user->email);
        $response->assertSeeText($user->profile->dni);
        $response->assertSeeText($user->profile->phone);
        $response->assertSeeText($user->profile->address);
    }

    

    /** @test */
    public function user_can_visit_create_profile_page()
    {
        $user = $this->createUser();
        $state = factory(State::class)->create();
        $sector = factory(Sector::class)->create(['state_id' => $state->id]);
        $profile = factory(Profile::class)->make(['sector_id' => $sector->id]);
        $user->profile()->save($profile);

        $response = $this->actingAs($user)->get(route('user.profile.create'));
        $response->assertStatus(200);
        $response->assertViewIs('user.profile.create');
        $response->assertViewHas(['profile', 'user']);
        $response->assertSeeText($user->name);
        $response->assertSeeText($user->lastname);
        $response->assertSeeText($user->email);
        
    }

    /** @test */
    public function user_can_create_profile()
    {
        $user = $this->createUser();
        $state = factory(State::class)->create();
        $sector = factory(Sector::class)->create(['state_id' => $state->id]);
        
        $data = [
            'dni' => '123456',
            'phone' => '1234567890',
            'address' => 'calle principal',
            'city' => 'Caracas',
            'sector_id' => $sector->id
        ];

        $response = $this->actingAs($user)
            ->post(route('user.profile.store'), $data);
        
        $response->assertRedirect(route('user.profile'));
    }

    /** @test */
    public function user_cannot_create_profile_with_invalid_data()
    {
        $user = $this->createUser();
        $data = [
            [ 'dni' => '' ],
            [ 'dni' => str_random(21) ],
            [ 'phone' => '' ],
            [ 'phone' => str_random(21) ],
            [ 'address' => '' ],
            [ 'address' => str_random(151)],
            [ 'city' => '' ],
            [ 'city' => str_random(151)],
            [ 'sector_id' => '' ],
            [ 'sector_id' => 'a' ],      
        ];
    
        $this->assertValidationsErrors($data, route('user.profile.store'), 'post', 'profiles', $user);
    }
}
