<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Requests\StorePhoto;

class VehicleImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index($vehicle)
    {
        $user = request()->user();
        $vehicle = Vehicle::where('id', $vehicle)
                    ->where('user_id', $user->id)
                    ->first();
        
        return [
            'vehicle_id' => $vehicle->id,
            'photos' => $vehicle->getMediaUrls('photos')
        ];
    }

    public function store(StorePhoto $request, $vehicle)
    {
        $user = request()->user();
        $vehicle = Vehicle::where('id', $vehicle)
                    ->where('user_id', $user->id)
                    ->first();
        $vehicle->addMediaFromRequest('photo')->toMediaCollection('photos');
        
        return response()->json(['saved' => true]);
    }

    public function delete($vehicle, $photo)
    {
        $user = request()->user();
        $vehicle = Vehicle::where('id', $vehicle)
                    ->where('user_id', $user->id)
                    ->first();
        $vehicle->deleteMedia($photo);
        
        return response()->json(['deleted' => true]);
    }
}
